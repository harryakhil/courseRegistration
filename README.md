# `courseRegistration` — Sample app for student course registration

This project is an application for online student course registration. 
Student can view and enroll a course and check the status of the course.

## Getting Started

To get you started you can simply clone the `courseRegistration` repository and build the project:

### Prerequisites

You need git to clone the `courseRegistration` repository. 
**Install maven(3.2.2)**
**Install Java 1.8**


### Clone `courseRegistration`

Clone the `courseRegistration` repository using git:


git clone https://gitlab.com/harryakhil/courseRegistration.git

cd courseRegistration


### Run the Application

In the command prompt run the below command:

**mvn spring-boot:run**


### Run the tests

In the command prompt run the below command:


**mvn test**

### View the database GUI:

I have used the H2 database(in memory) provided by Spring.
The database will be up and running once the application is started.
To access the database GUI, access  [h2-console](http://localhost:8080/h2-console).

To access the Rest API, USE [Swagger2](http://localhost:8080/swagger-ui.html).







